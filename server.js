const express = require("express")
const fs = require("fs")
const cors = require("cors")
const path = require("path")

const app = express()


const porta = 3001


app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())
app.use(express.static(path.join(__dirname, "img")));

app.listen(porta, function () {
    console.log(`Servidor rodando na porta ${porta}`);
})


app.get("/cargainicial", function (req, resp) {

    fs.readFile('config.csv', 'utf8', function (err, data) {

        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        }

        console.log(data);

        var candidato = data.split("\r\n")
        console.log(candidato);

        var candidatos = []



        candidato.forEach(element => {
            candidatos.push(element.split(","))
        });

        for (let i = 1; i < candidatos.length; i++) {
            candidatos[i][3] = `http://localhost:3001/Cand${i}.jpg`

        }

        console.log(candidatos);

        resp.send(candidatos)
    });

})




app.post("/voto", function (req, resp) {

    console.log(req.body.rg);
    console.log(req.body.numCandidato);
    console.log(req.body.horaVoto);

    let voto = `${req.body.rg},${req.body.numCandidato}, ${req.body.horaVoto}`
    fs.appendFile("votacao.csv", `${voto}\n`, function (err) {
        if (err) {
            resp.json({
                "status": 500,
                "mensagem": "Erro ao registrar voto, contate o administrador do sistema"
            })

        }
        else {
            resp.json({
                "status": 200,
                "mensagem": "Voto Registrado Com sucesso"
            })
        }
    })


})



app.get("/apuracao", function (req, resp) {
    fs.readFile("votacao.csv", "utf8", function (err, data) {
        var voto = data.split("\n")
        var votos = []
        voto.forEach(element => {
            votos.push(element.split(","))
        })

        var contagemVotos = [0, 0, 0, 0, 0, 0, -1]
        for (let i = 0; i < votos.length; i++) {
            switch (votos[i][1]) {
                case "1234":
                    contagemVotos[0]++
                    break;
                case "4321":
                    contagemVotos[1]++
                    break;
                case "2134":
                    contagemVotos[2]++
                    break;
                case "3214":
                    contagemVotos[3]++
                    break;
                case "3124":
                    contagemVotos[4]++
                    break;
                case "":
                    contagemVotos[5]++
                    break;
                default:
                    contagemVotos[6]++


            }



        }

        fs.readFile("config.csv", "utf8", function (err, data) {
            var candidato = data.split("\r\n")
            var apuracaoFinal = []

            candidato.forEach(element => {
                apuracaoFinal.push(element.split(","))
            })

            apuracaoFinal.push(["", "", "Branco", ""])
            apuracaoFinal.push(["", "", "Nulos", ""])


            //console.log(contagemVotos);
            for (let i = 1; i < apuracaoFinal.length; i++) {
                apuracaoFinal[i].shift() // remove item 0 de todas as linhas (coluna 0)
                apuracaoFinal[i].push(contagemVotos[i - 1]) // insere a contagem de votos no final

            }

            apuracaoFinal.shift() //remove linha cabeçalho dos dados
            apuracaoFinal.sort(ordenarApuracao) //ordena por qnt de votos

            //console.log(apuracaoFinal);
            resp.send(apuracaoFinal)
        })

    })


})


app.get("/apuracaoidentificada", function (req, resp) {
    fs.readFile("votacao.csv", "utf8", function (err, data) {
        var voto = data.split("\n")
        var votos = []
        voto.forEach(element => {
            votos.push(element.split(","))
        })

        var votosDuplicados
        var noIf = true

        while (noIf) {
            noIf = false
            for (let i = 0; i < votos.length - 1; i++) {
                if (votos[i][0] == votos[i + 1][0]) {
                    votosDuplicados = (votos.splice(i + 1, 1))
                    noIf = true

                }
            }
        }
        var votosValidos = votos

        var contagemVotos = [0, 0, 0, 0, 0, 0, -1]
        for (let i = 0; i < votos.length; i++) {

            switch (votos[i][1]) {
                case "1234":
                    contagemVotos[0]++
                    break;
                case "4321":
                    contagemVotos[1]++
                    break;
                case "2134":
                    contagemVotos[2]++
                    break;
                case "3214":
                    contagemVotos[3]++
                    break;
                case "3124":
                    contagemVotos[4]++
                    break;
                case "":
                    contagemVotos[5]++
                    break;
                default:
                    contagemVotos[6]++


            }

        }

        fs.readFile("config.csv", "utf8", function (err, data) {
            var candidato = data.split("\r\n")
            var apuracaoFinal = []

            candidato.forEach(element => {
                apuracaoFinal.push(element.split(","))
            })

            apuracaoFinal.push(["", "", "Branco", ""])
            apuracaoFinal.push(["", "", "Nulos", ""])


            for (let i = 1; i < apuracaoFinal.length; i++) {
                apuracaoFinal[i].shift() // remove item 0 de todas as linhas (coluna 0)
                apuracaoFinal[i].push(contagemVotos[i - 1]) // insere a contagem de votos no final

            }

            apuracaoFinal.shift() //remove linha cabeçalho dos dados
            apuracaoFinal.sort(ordenarApuracao) //ordena por qnt de votos

            //console.log(apuracaoFinal);
            resp.send(apuracaoFinal)
        })

    })

})

function ordenarApuracao(a, b) {
    if (a[3] > b[3]) {
        return -1
    }
    if (b[3] > a[3]) {
        return 1
    }
    else return 0

}
